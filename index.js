const mainButton = document.querySelector("body > main > button");

mainButton.addEventListener('click', () => {

    document.querySelector("body > header > section > form > input[type=email]").focus({ preventScroll: false });
});

const btnSubmit = document.querySelector("body > header > section > form > button");

btnSubmit.addEventListener('click', (evt) => {
    evt.preventDefault();

    const input = document.querySelector("body > header > section > form > input[type=email]");

    if (input.validity.valid) {

        toastr.success(`Email ${input.value} has been sent!`);
    } else {
        toastr.error(`Please enter a correct email!`)
    }


    input.value = '';
})